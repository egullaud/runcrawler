#!/usr/bin/env python
import json

from glob import glob
from pymongo import MongoClient
from bson.errors import InvalidDocument

client = MongoClient()
db = client["avbp_mining"]
log = db["log_monitoring"]

run_list = iter(sorted(glob("DATABASE/*")))

for path in run_list:
    print((" --- Treating " + path))
    entry = json.load(open(path))
    print(('test-------->', entry["log_monitoring"]))
    if "log_monitoring" in entry and entry["log_monitoring"]:
        log.insert_many(entry["log_monitoring"])
        
