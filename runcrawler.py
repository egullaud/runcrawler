import sys
import os
import json
import re
import hashlib
from glob import glob
from datetime import datetime
from os.path import join, dirname, basename, abspath, isfile

CLUSTER = "JADAWIN"
SEP = "__"


def checkold(dpath):
    run_list = iter(sorted(glob(dpath)))
    old_id = []
    for path in run_list:
        entry = json.load(open(path))
        for el in range(len(entry["log_monitoring"])):
            old_id.append(hashlib.md5(open(entry["log_monitoring"][el]['path'], 'rb').read()).hexdigest())

    return old_id


def main(root, dpath):
    opath = checkold(dpath)
    print('The following directories have been already read, runcrawler will skip them:', len(opath), opath)
    for dirp, dirn, filn in crawl(root):
        print(" --->", dirp)
        try:
            entry = create_entry(dirp, filn, opath)
        except Exception as err:
            print("\t*** Couldn't write json file.")
            print("\t\t", err)
            continue
        try:
            json.dump(entry,
                      open(join("DATABASE",
                                CLUSTER + dirp.replace("/", SEP) + ".json"),
                           "w"),
                      indent=4, separators=(',', ': '),
                      cls=CustomEncoder)
        # TODO: type d'error plus specifique
        except Exception as err:
            print("\t*** Couldn't write json file.")
            print("\t\t", err)
        sys.stdout.flush()


class CustomEncoder(json.JSONEncoder):
    def default(self, obj):
        try:
            obj = obj.tolist()
        except AttributeError:
            pass
        if len(obj) == 1: obj = obj[0]
        if type(obj) == str: obj = str(obj, errors="ignore")
        # Let the base class default method raise the TypeError
        return json.JSONEncoder.default(self, obj)


def raw_file(path, dump=True):
    contents = None
    print("\t> Accessing file at", path, isfile(path))
    if (path is None) or (not isfile(path)):
        print("\t*** No file there")
        return None

    if dump:
        try:
            with open(path) as f:
                contents = f.read()
        except IOError:
            print("\t*** Couldn't access this file")
            return None

    return contents


def create_entry(dirpath, filenames, opath):
    entry = {}
    entry.update(detect_files(dirpath, filenames, opath))
    return entry


def crawl(root):
    root = abspath(root)
    for dirpath, dirnames, filenames in os.walk(root):
        ## Todo: the three lines below are dependent on AVBP
        if "run.params" in filenames:
            if ((basename(dirpath) == "INPUT") & (basename(dirname(dirpath)) == "WORK")):
                continue
            yield dirpath, dirnames, filenames


def detect_files(dirname, filenames, opath):
    out_all = []
    for f in filenames:
        ## Todo: this part is avbp-dependent
        if f[-2:] != ".o" and f[-2:] != "run.params": continue
        fn = abspath(join(dirname, f))
        fn_id = hashlib.md5(open(fn, 'rb').read()).hexdigest()
        if fn_id in opath:
            print("this data already exists => skip")
            continue
        contents = raw_file(fn)
        if contents is None: continue

        if "AVBP version" in contents:
            out = {}
            out["path"] = str(fn)
            prog = parse_avbp_o(fn)
            di = {"creation_time": file_time(fn), "err_log": prog, "vars": read_variables(contents)}
            if "run.params" in filenames:
                fparam = dirname + "/run.params"
                cont_params = raw_file(fparam)
                di["params"] = read_parameters(cont_params)
            out["contents"] = di
            out_all.append(out)

    if not out_all:
        res = None
    else:
        res = {"log_monitoring": out_all}
    print("log_monitoring:", res)

    return res


def file_time(fname):
    out = []
    try:
        out = datetime.fromtimestamp(os.path.getctime(fname)).strftime('%Y-%m-%dT%H:%M:%S')
    except Exception as err:
        print("\t*** Couldn't read the creation time.")
        print("\t\t", err)

    return out


def parse_avbp_o(fname):
    ## Todo: this part is avbp-dependent
    out = None
    err_list = {"Reading run parameters": ['before run params', 110],
                "Reading boundary conditions": ['run params ok,\n before asciibound file', 120],
                "Info on initial grid": ['asscibound ok,\n before grid', 130],
                "Writes full parameters file": ['grid ok,\n before init', 140],
                "Info on initial solution": ['init ok,\n before solutbound', 150],
                "Reading probe coordinates file": ['solutbound ok,\n before probes', 160],
                "Writing probe reference file": ['probes ok,\n before wall dist', 210],
                "Wall distances stored in file": ['wall dist ok,\n before other preproc', 290],
                "Starts the temporal loop": ['before temporal loop', 300],
                "Integral values for boundary patches": ['loop finished,\n before wrap up', 900],
                "End of AVBP session": ['before wrap up\n ok', 000]}

    with open(fname, "r") as f:
        for item in list(err_list.items()):
            f.seek(0)
            try:
                if item[0] in f.read():
                    out = item[1]
            except Exception as err:
                print("\t*** Couldn't read the target file.")
                print("\t\t", err)
                continue
    return out


def parse_variables_o(variable, reg, content):
    str = r'(.*?{}.*?)\n'.format(variable)
    line = re.search(str, content).group(0)
    print('line {}'.format(variable), line)
    print(re.findall(reg, line)[0])
    return re.findall(reg, line)[0]


def parse_variables_rp(variable, reg, content):
    str = r'(.*?{}.*?)\n'.format(variable)
    line = re.search(str, content).group(0)
    print('line {}'.format(variable), line)
    print(re.findall(reg, line)[1])
    return re.findall(reg, line)[1]


def read_variables(content):
    ## Todo: users need to specify "line" and "out"
    out = {}
    # AVBP version
    try:
        out['AVBP_version'] = parse_variables_o('AVBP version', r'\d.\d.\d', content)
    except Exception as err:
        print("\t*** No AVBP version in *.o file")
        print("\t\t", err)

    # number of nodes
    try:
        out['nodes'] = int(parse_variables_o('number of nodes',
                                             r'\d+', content))
    except Exception as err:
        print("\t*** No number of nodes in *.o file")
        print("\t\t", err)

    # number of cells
    try:
	out['mesh_cells'] = int(parse_variables_o('number of cells',
					'\d+', content))
    except Exception as err:
        print("\t*** No number of cells in *.o file")
        print("\t\t", err)	



    # nb of cores
    try:
        out['MPI_processes'] = int(parse_variables_o('MPI processes',
                                                     r'\d+', content))
    except Exception as err:
        print("\t*** No number of nodes in *.o file")
        print("\t\t", err)

    # dimension of domain
    try:
        out['dimension'] = int(parse_variables_o('dimension', r'\d+', content))
    except Exception as err:
        print("\t*** No number of nodes in *.o file")
        print("\t\t", err)

    # initial iteration
    try:
        out['initial_iteration'] = int(parse_variables_o('initial iteration', r'\d+', content))
    except Exception as err:
        print("\t*** No number of nodes in *.o file")
        print("\t\t", err)

    # final iteration
    try:
        out['final_iteration'] = int(parse_variables_o('Final iteration', r'\d+', content))
    except Exception as err:
        print("\t*** No number of nodes in *.o file")
        print("\t\t", err)

    # initial time
    try:
        out['initial_time'] = float(parse_variables_o('initial time', r'-?[\d.]+(?:E-?\d+)?', content))
    except Exception as err:
        print("\t*** No number of nodes in *.o file")
        print("\t\t", err)

    # physical time
    try:
        out['physical_time'] = float(parse_variables_o('dtsum', r'-?[\d.]+(?:E-?\d+)?', content))
    except Exception as err:
        print("\t*** dtsum in *.o file")
        print("\t\t", err)

    # elapsed time
    try:
        out['elapsed_time'] = float(parse_variables_o('AVBP                   ',
                                                      r'-?[\d.]+(?:E+?\d+)?', content))
    except Exception as err:
        print("\t*** No number of nodes in *.o file")
        print("\t\t", err)

    # CPU time
    try:
        out['CPU_time_s'] = float(parse_variables_o('AVBP                   ',
                                                    r'-?[\d]+.[\d]+E+\+\d+', content))
    except Exception as err:
        print("\t*** No number of nodes in *.o file")
        print("\t\t", err)

    # time per iteration (s)

    try:
        out['time_per_ite'] = float(parse_variables_o('Per iteration',
                                                      r'-?[\d.]+(?:E-?\d+)?', content))
    except Exception as err:
        print("\t*** No time per iteration  output  in *.o file")
        print("\t\t", err)
    # number of probes
    try:
        out['number_of_probes'] = int(parse_variables_o('number of probes', r'\d+', content))
    except Exception as err:
        print("\t*** No probes specified in *.o file")
        print("\t\t", err)

    return out


def read_parameters(content):
    out = {}

    # CFL
    try:
        out['CFL'] = parse_variables_rp('CFL', r'\d+', content)
    except Exception as err:
        print("\t*** no CFL specified in *.o file")
        print("\t\t", err)

    # artificial_viscosity_2nd_order
    try:
        out['smu2'] = parse_variables_rp('artificial_viscosity_2nd_order', r'\d+', content)
    except Exception as err:
        print("\t*** no smu2 specified in *.o file")
        print("\t\t", err)

    try:
        out['smu4'] = parse_variables_rp('artificial_viscosity_4nd_order', r'\d+', content)
    except Exception as err:
        print("\t*** no smu4 specified in *.o file")
        print("\t\t", err)

    # chemistry
    try:
        out['mixture_name'] = parse_variables_rp('mixture_name', r'\w+', content)
    except Exception as err:
        print("\t*** No mixture name specified in run.params file")
        print("\t\t", err)

        # solver type
    try:
        out['solver_type'] = parse_variables_rp('solver_type', r'\w+', content)
    except Exception as err:
        print("\t*** No solver_type specified in run.params file")
        print("\t\t", err)

        # LES_model
    try:
        out['LES_model'] = parse_variables_rp('LES_model', r'\w+', content)
    except Exception as err:
        print("\t*** LES_model specified in run.params file")
        print("\t\t", err)

        # artificial viscosity model
    try:
        out['artificial_viscosity_model'] = parse_variables_rp('artificial_viscosity_model', r'\w+', content)
    except Exception as err:
        print("\t*** No artificial_viscosity_model specified in run.params file")
        print("\t\t", err)

        # convection scheme
    try:
        out['convection_scheme'] = parse_variables_rp('convection_scheme', r'\w+', content)
    except Exception as err:
        print("\t*** No convection_scheme specified in run.params file")
        print("\t\t", err)

        # hpc details
    # ncell
    try:
        out['ncell_group'] = parse_variables_rp('ncell_group', r'\w+', content)
    except Exception as err:
        print("\t*** No ncell_group specified in run.params file")
        print("\t\t", err)

        # partitioner
    # convection scheme
    try:
        out['partitioner'] = parse_variables_rp('partitioner', r'\w+', content)
    except Exception as err:
        print("\t*** No partitioner specified in run.params file")
        print("\t\t", err)

        # reactive flow
    try:
        out['reactive_flow'] = (1 if parse_variables_rp('reactive_flow', r'\w+', content) == 'yes' else 0)
    except Exception as err:
        print("\t*** No reactive flow specified in run.params file")
        print("\t\t", err)

        # Two-phased flow or not
    try:
        out['two_phase_flow'] = (1 if parse_variables_rp('two_phase_flow', r'\w+', content) == 'yes' else 0)
    except Exception as err:
        print("\t*** No two phase flow specified in run.params file")
        print("\t\t", err)

        # real gas
    try:
        out['real_gas'] = (1 if parse_variables_rp('real_gas', r'\w+', content) == 'yes' else 0)
    except Exception as err:
        print("\t*** No real gas specified in run.params file")
        print("\t\t", err)

    return out

    return out


if __name__ == '__main__':
    #root = '/archive/cfd/rossi/'
    #root = '/scratch/cfd/dupuy' 
    #root = '/scratch/coop/gullaud/WKDIR'
    #root = '/scratch/cfd/pouech'
    root = '/scratch/cfd/potier'
    dpath = "./DATABASE/*"
    main(root, dpath)
