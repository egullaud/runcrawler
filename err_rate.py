#!/usr/bin/env python
import json

from glob import glob
from pymongo import MongoClient
import matplotlib.pyplot as plt

client = MongoClient()

with client:
    db = client.avbp_mining.log_monitoring

    nums = db.count()
    clist = list(db.aggregate(
            [
               {"$match" : { "contents.creation_time" : {
                "$gte": "2010-10-16 16:31:29",
                "$lt": "2020-10-16 16:31:29"}}},
               {"$match" : { "contents.vars.avbp_version" : {
                "$gte": 650,
                "$lt": 760}}},
               {"$match" : { "contents.vars.tag.TTGC" : True }},                
               { "$group": { "_id": {"err_log": {"$substr": [{ "$arrayElemAt": [ "$contents.err_log", 1 ]}, 0, 1]} }, "count": { "$sum": 1 }}}
            ]
            ))



err_type = []
count = []
print(clist)

for i in range(len(clist)):
    err_type.append(clist[i]['_id']['err_log'])
    count.append(int(clist[i]['count']))

err_type = list(map(( lambda x: x + "**"), err_type))
print('yest', err_type)
fig1, ax1 = plt.subplots()
ax1.pie(count, labels=err_type, autopct='%1.1f%%')
ax1.axis('equal')
ax1.set_title('Type of error')
plt.savefig('err_type.png')



