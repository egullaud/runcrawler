#!/usr/bin/env python
"""
Convert AVBP 7.0 run.params file to JSON format

Created Jan 2017 by C. Lapeyre (lapeyre@cerfacs.fr)
"""
USAGE = """\
    USAGE:
        params2json run.params
    """

import json

from collections import OrderedDict

def params2json(params_file):
    with open(params_file) as f:
        lines = f.readlines()
        blocks = [i for i, line in enumerate(lines)
                  if '$' in line]
        data = OrderedDict()
        for start, end in zip(blocks[::2], blocks[1::2]):
            block_key = lines[start].strip('$\n ')
            d = OrderedDict()
            for line in lines[start+1:end]:
                line = line.split("!")[0]
                if not line.strip(): continue
                try:
                    key, value = line.strip().split('=')
                except Exception:
                    print("\t*** Couldn't read line. Skipping...")
                    print("\t\t", line)
                    continue
                value = value.strip()
                if value[0].isdigit():
                    value = value.upper()
                    if ' ' in value:
                        l = value.split()
                        if 'D' in l[0]:
                            value = [float(f.replace('D', 'E')) for f in l]
                        else:
                            value = [int(i) for i in l]
                    else:
                        if 'D' in value:
                            value = float(value.replace('D', 'E'))
                        else:
                            value = int(value)
                d[key.strip()] = value
            data[block_key] = list(d.items())
    return list(data.items())

if __name__ == '__main__':
    import sys
    if len(sys.argv[1:]) != 1:
        print(USAGE)
        sys.exit()

    json.dump(params2json(sys.argv[1]), sys.stdout,
              indent=4, separators=(',', ': '))

            

